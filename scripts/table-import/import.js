/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable function-paren-newline */
/* eslint-disable implicit-arrow-linebreak */
/* eslint-disable comma-dangle */
/* eslint-disable brace-style */

const readFilesScript = require('../read-files/read-files-script');

const fsp = require('fs').promises;
const { GoogleSpreadsheet } = require('google-spreadsheet');
const readlineSync = require('readline-sync');

const creds = require('./AccountAuth.json');

const { renameCviceni } = require('../rename-cviceni-function');

const templateCviceni = require('../../src/scaffolding-templates/cviceni/data/cviceniNewData.json');

// Command line arguments
const indexIdOfCviceni = process.argv.indexOf('--id');

let inputIdOfCviceni = process.argv[indexIdOfCviceni + 1];
// Setup names
if (indexIdOfCviceni === -1) {
  inputIdOfCviceni = '';
}

/**
 * Main method to rewrite files in cviceni
 * @param {JSON} exportVelkaTabulka table of imported data from Velka Tabulka
 */
function rewriteCviceniWithExportOfVelkaTabulka(exportVelkaTabulka) {
  /**
   *
   * @param {Path} dirname path to specific cviceni JSON
   * @param {String} filename name of the specific cviceni JSON file
   * @param {JSON} data specific data of the cviceni
   */
  async function manageData(dirname, filename, data) {
    // Must be init again becouse ESLint and renaming cviceni
    const cviceniData = JSON.parse(JSON.stringify(templateCviceni.cviceni));

    // Find cviceni in ImportedVelkaTabula JSON
    const findedData = await exportVelkaTabulka.find(
      (velkaTabulka) =>
        parseInt(velkaTabulka.id, 10) === parseInt(data.cviceni.id, 10)
    );

    // If local data was found in Velka Tabulka, start renaming
    if (findedData) {
      if (!findedData.zverejneni) {
        console.log(
          `Cviceni ${filename} has not been published (in test) yet!`
        );
        return;
      }

      // ================Copy-version====================
      cviceniData.version = data.cviceni.version;

      // ================Copy-ID====================
      cviceniData.id = data.cviceni.id;

      // ===============Katalog============
      cviceniData.katalog = data.cviceni.katalog;

      // ===============Jazyk============
      cviceniData.language = data.cviceni.language;

      // ===============Slug============
      cviceniData.slug = data.cviceni.slug;

      // ===============Nazev============
      cviceniData.nazev = data.cviceni.nazev;

      // ====================Autor====================
      if (findedData.autor) {
        const authorsArray = findedData.autor.split(', ');
        cviceniData.autor = authorsArray;
      }

      // ====================Klicova-slova====================
      // -- RVP --
      const rvp = [];
      if (findedData.KSrvp1) {
        const rvp1 = findedData.KSrvp1.trim();
        if (rvp1 !== '') {
          rvp.push(rvp1);
        }
      }
      if (findedData.KSrvp2) {
        const rvp2 = findedData.KSrvp2.trim();
        if (rvp2 !== '') {
          rvp.push(rvp2);
        }
      }
      cviceniData.klicovaSlova['rvp'] = rvp;
      // -- Konceptove --
      const konceptove = [];
      if (findedData.KSkonceptove1) {
        const konceptove1 = findedData.KSkonceptove1.trim();
        if (konceptove1 !== '') {
          konceptove.push(konceptove1);
        }
      }
      if (findedData.KSkonceptove2) {
        const konceptove2 = findedData.KSkonceptove2.trim();
        if (konceptove2 !== '') {
          konceptove.push(konceptove2);
        }
      }
      cviceniData.klicovaSlova['koncept'] = konceptove;
      // -- B4 --
      if (findedData.KSB4) {
        const B4 = findedData.KSB4.trim();
        if (B4 !== '') {
          cviceniData.klicovaSlova['b4'] = [B4];
        }
      }
      // -- Historylab
      if (findedData.KShistorylab) {
        const historylabove = findedData.KShistorylab.trim();
        if (historylabove !== '') {
          cviceniData.klicovaSlova['historylab'] = [historylabove];
        }
      }

      // ====================Anotace====================
      if (findedData.anotaceProUcitele) {
        cviceniData.anotace.ucitel = findedData.anotaceProUcitele.trim();
      }
      if (findedData.anotaceProZaky) {
        cviceniData.anotace.verejna = findedData.anotaceProZaky.trim();
      }

      // ====================Doba-trvani====================
      if (findedData.dobaTrvani) {
        const trvaniInt = findedData.dobaTrvani.split(' ');
        // eslint-disable-next-line prefer-destructuring
        try {
          cviceniData.trvani = parseInt(trvaniInt[0], 10);
        } catch (e) {
          cviceniData.trvani = 0;
        }
      }

      // ====================Funkce====================
      cviceniData.funkce = data.cviceni.funkce;

      // ====================PDF====================
      cviceniData.pdf = data.cviceni.pdf;

      // ====================UvodniObrazek====================
      cviceniData.uvodniObrazek = data.cviceni.uvodniObrazek;

      // ====================Obtiznost====================
      if (findedData.obtiznost) {
        cviceniData.obtiznost = findedData.obtiznost.trim();
      }

      // ====================CASOVA OSA====================
      // ------------------Cas------------------

      if (findedData.roky) {
        const arrayOfRoks = findedData.roky
          .toString()
          .trim()
          .split(', ')
          .map((element) => parseInt(element, 10));

        cviceniData.casovaOsa.roky = arrayOfRoks;
      }

      // ------------------Epochy------------------

      // rozdělení pro více epoch
      if (findedData.epochaCeska) {
        const arrayOfCeskeEpoch = findedData.epochaCeska.trim()
          .split(', ');
        cviceniData.casovaOsa.epochy[0].obdobi = arrayOfCeskeEpoch;
      }

      if (findedData.epochaSvetova) {
        const arrayOfWorldEpoch = findedData.epochaSvetova.trim()
          .split(', ');
        cviceniData.casovaOsa.epochy[1].obdobi = arrayOfWorldEpoch;
      }

      // ====================ADD-color===========================
      const getImageColorProperties = await require('../visual-cloud/get-image-color-properties');
      cviceniData.color = await getImageColorProperties(`src/img/${data.cviceni.uvodniObrazek}`);

      // =====================ADD-template-to-cviceni====================
      data.cviceni = cviceniData;

      // =====================Rewrite-the-file=====================
      await fsp.writeFile(
        dirname + filename,
        JSON.stringify(data, null, 2),
        (err) => {
          if (err) return console.error(err);
          return true;
        }
      );

      // =====================Rename-cviceni=====================
      if (
        findedData.nazev.trim()
        && findedData.nazev.trim() !== data.cviceni.nazev
      ) {
        // Wait for user's response.
        const confirm = readlineSync.question(
          `Do you want to rename cviceni ${
            data.cviceni.nazev
          } to ${findedData.nazev.trim()}? [yes]/no:`
        );
        if (confirm !== 'no') {
          await renameCviceni(data.cviceni.nazev, findedData.nazev.trim())
            .then((result) => {
              if (result) {
                console.log(
                  `====CVICENI-WAS-SUCCESSFULLY-RENAMED from: "${
                    data.cviceni.nazev
                  }" to:"${findedData.nazev.trim()}"`
                );
              }
              else {
                console.log(
                  `====CVICENI-WAS-NOT-RENAMED: "${data.cviceni.nazev}"`
                );
              }
            })
            .catch();
        }
        else {
          console.log(
            `Cviceni ${data.cviceni.nazev} has different name use script change-name or re-run this script and confirm dialog for renaming cviceni`
          );
        }
      }
    }
  }

  readFilesScript(
    'src/data//',
    (dirname, filename, content) => {
      // Check if the given data can be parsed into JSON
      try {
        const type = filename.split('.');
        if (type[1] !== 'json') {
          console.log(`File: ${filename} is not a json -> SKIPPING`);
          return;
        }
        const contentParsed = JSON.parse(content);

        if (inputIdOfCviceni) {
          if (contentParsed.cviceni.id !== parseInt(inputIdOfCviceni, 10)) {
            return;
          }
          manageData(dirname, filename, contentParsed);
        }
        else {
          manageData(dirname, filename, contentParsed);
        }

        // manageData(dirname, filename, contentParsed);
      } catch (e) {
        console.log(`Error pri ukladani v cviceni: ${filename}\n${e}\n`);
      }
    },
    (err) => {
      throw err;
    }
  );
}

/**
 * Setup google Auth
 * spreadsheet key is the long id in the sheets URL
 */
async function accessSpreadsheet() {
  console.log(
    '=======================STARTING-CONFIGURATION================================='
  );
  const velkaTabulkaExportJSON = [];
  const doc = new GoogleSpreadsheet(
    '1Z_sicl0T7ROHQINXGB_pkuqjZZlMHD2_0Y6nudIvn4U'
  );

  await doc.useServiceAccountAuth({
    client_email: creds.client_email,
    private_key: creds.private_key,
  });

  // Must call loadInfo() -> works like lazy load
  await doc.loadInfo();
  // Access spredsheet DevelopersList (which has number 1659665612 - viz url)
  const sheet = doc.sheetsById[1659665612];

  console.log(
    '=======================LOADING-DATA-FROM-VELKA-TABULKA================================='
  );
  // Lazy load for load rows
  const rows = await sheet.getRows();
  // Get header values
  const headerOfTable = sheet.headerValues;

  // Prepare given data from Velka Tabulka into JSON format
  rows.forEach((row) => {
    const cviceni = {};
    headerOfTable.forEach((header, index) => {
      cviceni[header] = row._rawData[index];
    });
    velkaTabulkaExportJSON.push(cviceni);
  });

  console.log(
    '=======================REWRITING-CVICENI================================='
  );
  // Call main function with exported data in JSON format
  rewriteCviceniWithExportOfVelkaTabulka(velkaTabulkaExportJSON);
}

// Method to start auth, import and remake files from Velka Tabulka
accessSpreadsheet()
  .catch((err) =>
    console.error(`Something went wrong with import. ${err}`)
  );
